toggle\_flags.vim
=================

This plugin provides `:ToggleFlag` and `:ToggleFlagLocal` commands to toggle
the values of options like `'formatoptions'` or `'complete'` that have values
comprised of single-character or comma-separated flags.  The author originally
designed it for toggling flags in `'formatoptions'` quickly.

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
