" Internal function to do the toggling
function! toggle_flags#(option, flag, local) abort

  " Fatal errors in this block just get quietly reported to the user
  try

    " Check option exists
    if !exists('&'.a:option)
      echoerr 'No such option '.a:option
    endif

    " Don't allow blank flag
    if !strlen(a:flag)
      echoerr 'Blank flag'
    endif

    " Get option's current value.  Is there a way to do this without :execute?
    " I couldn't make it work with :help curly-braces-names.
    let value = ''
    execute 'let value = &'.a:option

    " Figure out whether the flag is presently enabled in the option or not;
    " if it's longer than a single character, look for a comma-delimited word
    let enabled = strlen(a:flag) > 1
          \ ? stridx(','.value.',', ','.a:flag.',') != -1
          \ : stridx(value, a:flag) != -1

    " Build and run command
    execute (a:local ? 'setlocal ' : 'set ')
          \ . a:option
          \ . (enabled ? '-=' : '+=')
          \ . escape(a:flag, '\ ')

    " Display option's new value
    execute 'set '.a:option.'?'

  catch

    " Print fatal error as just error message highlighted text, so that it
    " only takes up one line and doesn't inspire Enter keypresses and panic
    echohl ErrorMsg
    echomsg v:exception
    echohl None

  endtry

endfunction
