"
" toggle_flags.vim: Provide commands to toggle flags in grouped options like
" 'formatoptions', 'shortmess', 'complete', 'switchbuf', etc.
"
" Author: Tom Ryder <tom@sanctum.geek.nz>
" License: Same as Vim itself
"
if exists('loaded_toggle_flags') || &compatible || v:version < 700
  finish
endif
let loaded_toggle_flags = 1

" User commands wrapping around calls to the above function
command -bar -nargs=+ -complete=option ToggleFlag
      \ call toggle_flags#(<f-args>, 0)
command -bar -nargs=+ -complete=option ToggleFlagLocal
      \ call toggle_flags#(<f-args>, 1)
